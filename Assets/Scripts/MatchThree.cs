﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MatchThree : MonoBehaviour
{
    #region public members

    public Piece tilePrefab;
    public PointBurst pointBurstPrefab;
    public Projectile projectilePrefab;

    public int height = 8;
    public int width = 8;
    public float xOffset = 1;
    public float yOffset = 1;
    public float boardXOffset = 1;
    public float boardYOffset = 1;
    public Piece[,] grid;
    public float moveSpeed = 1;

    public UnityEngine.UI.Text scoreText;
    public UnityEngine.UI.Text bossHealthText;
    public UnityEngine.UI.Text playerHealthText;
    public UnityEngine.UI.Text bossDPSText;
    public UnityEngine.UI.Text playerSpecialText;
    public UnityEngine.UI.Text playerGoldText;
    public UnityEngine.UI.Text overlayText;
    public UnityEngine.UI.Button overlayButton;
    public UnityEngine.UI.Image bossHealthBar;
    public UnityEngine.UI.Image bossSpecialBar;
    public UnityEngine.UI.Image playerHealthBar;
    public UnityEngine.UI.Image playerSpecialBar;
    public UnityEngine.UI.Image shieldIndicator;
    public UnityEngine.UI.Image overlay;

    public UnityEngine.Color announceColour = new Color(0.9f, 0.9f, 0.1f, 0.7f);
    public ParticleSystem selectParticle;
    public ParticleSystem playerSpecialParticle;
    public ParticleSystem bossSpecialParticle;
    public AudioClip gemSelect;
    public AudioClip gemSwap;
    public AudioClip gameOver;
    public AudioClip invalidSwap;
    public AudioClip reset;
    public AudioClip sword;
    public AudioClip shield;
    public AudioClip potion;
    public AudioClip coin;
    public AudioClip laser;
    public AudioSource backgroundMusic;

    #region Player/Boss/Game settings
    /*
        A whole lot of settings for changing how the game behaves and affecting real and perceived difficulty
    */
    // note self : Should refactor these Player and Boss properties to Objects
    // Player properties
    public GameObject player;                  //reference to the player avatar in the scene
    public int playerMaxHealth = 300;          //player initial and maximum health amount
    public int playerAttack = 20;              //player's attack modifier
    public int playerSpecial = 25;             //player's special attack modifier
    public int playerSpecialPerMatch = 100;     //amount of special attack charge player gets per match
    public int playerSpecialModifier = 6;      //damage multiplier for special attack 
    public int playerGoldGain = 10;            //amount of gold player gains for a gold match
    public int playerGoldGainRate = 5;         //amount that the gold gain amount increases with each match

    private int playerHealth = 0;               //player's current health amount
    private int playerGold = 0;                 //player's gold amount
    private int playerSpecialCharge = 0;        //player's current amount of special charge 

    // Boss properties
    public GameObject boss;                    //reference to the boss avatar in the scene
    public int bossInitDPS = 1;                //the DPS (damage per second) initially dealt by the boss to the player
    public int bossDPSRampRate = 5;            //how many seconds before the boss DPS increases
    public int bossMaxHealth = 1000;           //boss's maximum and starting health

    private int bossDPS = 0;                    //the current DPS (damage per second) dealt by the boss to the player
    private int bossHealth = 0;                 //the boss's current health amount
    private int bossDPSRamp = 0;                //counter for tracking whether to increase boss DPS (checked each second)

    // Game properties
    public int baseHeal = 20;                  //amount of health healed to the player when potions are match
    public int shieldAmount = 2;               //amount of time shield duration gains when matched
    public int spawnSpecialShots = 0;          //fire this amount of extra lasers when player gets a special
    public int specialShots = 10;              //lasers remaining to be fired from player getting a special
    public float extraShotRate = 2f;           //time between extra shots from special

    private float extraShotCounter = 0f;        //counter used to track and fire more shots
    private float shieldCount = 0f;             //amount of seconds remaining on player shield
    private int shieldMax = 1;                  //(current) maximum shield time


    #endregion

    #endregion

    #region private members
    // different game states
    private enum State
    {
        Waiting,
        Swapping,
        Dropping,
        Matching,
        Paused,
        Ended
    }

    // active and paused states
    private State state;
    private State pauseState;                   //holds the gamestate while paused for smooth resume

    //UI Properties
    private Boolean overlayOn = false;          //flag for the UI overlay
    private Boolean firstTimeHealthDrop = true; //flag indicating whether player health has dropped below 25% yet this game

    // structure for indexing cells
    private struct CellOffset
    {
        public CellOffset(int col, int row)
        {
            Col = col;
            Row = row;
        }


        public int Col;
        public int Row;
        public static CellOffset operator +(CellOffset c1, CellOffset c2)
        {
            return new CellOffset(c1.Col + c2.Col, c1.Row + c2.Row);
        }
    }

    // pattern match class
    private class Pattern
    {

        public CellOffset[] MustHaves { get { return mustHaves; } }
        public CellOffset[] NeedOne { get { return needOne; } }

        private CellOffset[] mustHaves;
        private CellOffset[] needOne;

        public Pattern(int[,] mustHaves, int[,] needOne)
        {
            this.mustHaves = new CellOffset[mustHaves.Length];
            for (int i = 0; i < mustHaves.GetLength(0); i++)
            {
                this.mustHaves[i] = new CellOffset(mustHaves[i, 0], mustHaves[i, 1]);
            }
            this.needOne = new CellOffset[needOne.Length];
            for (int i = 0; i < needOne.GetLength(0); i++)
            {
                this.needOne[i] = new CellOffset(needOne[i, 0], needOne[i, 1]);
            }
        }

    }

    // list of all pattern match patterns
    private Pattern[] matchPatterns;

    // active score
    private int score = 0;

    // selected piece
    private Piece firstPiece;

    // random number generator
    private System.Random rand = new System.Random();

    // audiosource
    private AudioSource audio;

    #endregion

    #region Unity methods
    // Use this for initialization
    void Start()
    {
        SetupMatchPatterns();
        grid = new Piece[width, height];

        audio = GetComponent<AudioSource>();
        // Set up child gems
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                AddPiece(col, row);
            }
        }

        // create an interesting pattern - one that contains no matches, 
        // but does contain potential matches 
        do
        {
            // randomise board
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    grid[col, row].PieceType = rand.Next(Piece.MAX_PIECES);
                }
            }

        } while (ContainsMatch() || !ContainsPossibles());

        // setup game variables and initial UI displays
        overlayButton.interactable = false;
        boss = GameObject.Find("BossAvatar");
        player = GameObject.Find("PlayerAvatar");
        bossDPS = bossInitDPS;
        bossHealth = bossMaxHealth;
        playerHealth = playerMaxHealth;
        bossDPSText.text = (bossDPS > 0 ? ((bossDPS / bossInitDPS) * 10).ToString() + "%" : "0%");
        bossHealthText.text = bossHealth.ToString();
        playerHealthText.text = playerHealth.ToString();
        playerSpecialText.text = "0%";
        playerGoldText.text = playerGold.ToString();
        // start running boss damages player method once/sec
        InvokeRepeating("DamagePlayer", 1, 1);

    }

    private void Update()
    {
        // update UI indicators
        FillAllIndicators();

        //move pieces if any are falling
        if (state == State.Dropping || state == State.Swapping)
        {
            MovePieces();
        }

        // check for pause
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
        // check if need to generate laser projectiles
        if (spawnSpecialShots > 0)
        {
            extraShotCounter -= 0.1f;
            if (extraShotCounter < 0)
            {
                // generate another projectile
                extraShotCounter = extraShotRate;
                spawnSpecialShots--;
                audio.PlayOneShot(laser);
                FireProjectile(1);
            }
        }
    }
    #endregion

    #region Match3 Game Logic
    // this section has only had minor changes

    // add a piece to the board at the given row and column
    private Piece AddPiece(int col, int row)
    {
        Piece p = Instantiate(tilePrefab);
        p.transform.parent = transform;
        p.transform.localPosition = GetTargetPos(col, row);
        grid[col, row] = p;
        p.Row = row;
        p.Col = col;
        return p;
    }


    // get position for row/col
    Vector3 GetTargetPos(int col, int row)
    {
        return new Vector3((col * xOffset) + boardXOffset, (-row * yOffset) + boardYOffset, 0);
    }


    /// <summary>
    /// All possible patterns for a match
    /// </summary>
    private void SetupMatchPatterns()
    {

        matchPatterns = new Pattern[4];
        // horizontal possible, two plus one

        matchPatterns[0] = new Pattern(new int[,] { { 1, 0 } }, new int[,] { { -2, 0 }, { -1, -1 }, { -1, 1 }, { 2, -1 }, { 2, 1 }, { 3, 0 } });

        // horizontal possible, middle
        matchPatterns[1] = new Pattern(new int[,] { { 2, 0 } }, new int[,] { { 1, -1 }, { 1, 1 } });

        // vertical possible, two plus one
        matchPatterns[2] = new Pattern(new int[,] { { 0, 1 } }, new int[,] { { 0, -2 }, { -1, -1 }, { 1, -1 }, { -1, 2 }, { 1, 2 }, { 0, 3 } });

        // vertical possible, middle
        matchPatterns[3] = new Pattern(new int[,] { { 0, 2 } }, new int[,] { { -1, 1 }, { 1, 1 } });
    }


    /// <summary>
    /// Does the board contain a match
    /// </summary>
    /// <returns></returns>
    private bool ContainsMatch()
    {
        // search for horizontal matches
        for (int row = 0; row < height; row++)
        {
            int type = -1;
            int count = 0;
            for (int col = 0; col < width; col++)
            {
                var gem = grid[col, row];
                if (gem != null)
                {
                    if (gem.PieceType == type)
                    {
                        count++;
                        if (count == 3)
                            return true;
                    }
                    else
                    {
                        type = grid[col, row].PieceType;
                        count = 1;
                    }
                }
                else
                {
                    type = -1;
                    count = 0;
                }
            }
        }


        // search for vertical matches
        for (int col = 0; col < width; col++)
        {
            int type = -1;
            int count = 0;
            for (int row = 0; row < height; row++)
            {

                if (grid[col, row].PieceType == type)
                {
                    count++;
                    if (count == 3)
                        return true;
                }
                else
                {
                    type = grid[col, row].PieceType;
                    count = 1;
                }

            }
        }
        return false;
    }


    /// <summary>
    /// Find all of the matches on the board
    /// </summary>
    /// <returns></returns>
    private List<List<Piece>> AllMatches()
    {
        var matches = new List<List<Piece>>();
        // search for horizontal matches
        for (int row = 0; row < height; row++)
        {

            List<Piece> match = null;

            for (int col = 0; col < width; col++)
            {
                CheckMatch(matches, col, row, ref match);

            }
        }


        // search for vertical matches
        for (int col = 0; col < width; col++)
        {
            List<Piece> match = null;
            for (int row = height - 1; row >= 0; row--)
            {

                CheckMatch(matches, col, row, ref match);
            }
        }
        return matches;
    }


    /// <summary>
    /// Check for a match when collecting matches
    /// </summary>
    /// <param name="matches"></param>
    /// <param name="col"></param>
    /// <param name="row"></param>
    /// <param name="match"></param>
    private void CheckMatch(List<List<Piece>> matches, int col, int row, ref List<Piece> match)
    {
        Piece gem = grid[col, row];
        if (gem != null)
        {
            if (match != null && match.Count > 0 && gem.PieceType == match[0].PieceType)
            {
                match.Add(gem);
                if (match.Count == 3)
                {
                    matches.Add(match);
                }
            }
            else
            {
                match = new List<Piece>();
                match.Add(gem);
            }
        }
        else
        {
            match = null;
        }
    }


    /// <summary>
    /// Does the board contain possible matches
    /// </summary>
    /// <returns></returns>
    private bool ContainsPossibles()
    {
        for (int col = 0; col < width; col++)
        {
            for (int row = 0; row < height; row++)
            {
                // check for possible matches
                CellOffset cell = new CellOffset(col, row);
                for (int i = 0; i < matchPatterns.Length; i++)
                {
                    if (CanMatchPattern(cell, matchPatterns[i]))
                    {
                        return true;
                    }
                }
            }
        }
        // no possible moves found
        return false;
    }

    /// <summary>
    /// Check to see if a cell matches a pattern of a potential match
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="pattern"></param>
    /// <returns></returns>
    private bool CanMatchPattern(CellOffset pos, Pattern pattern)
    {
        int thisType = grid[pos.Col, pos.Row].PieceType;

        // make sure this has all must-haves
        for (int i = 0; i < pattern.MustHaves.Length; i++)
        {
            if (!MatchType(pos + pattern.MustHaves[i], thisType))
            {
                return false;
            }
        }

        // make sure it has at least one need-ones
        for (int i = 0; i < pattern.NeedOne.Length; i++)
        {
            if (MatchType(pos + pattern.NeedOne[i], thisType))
            {
                return true;
            }
        }
        return false;
    }

    // Does this cell contain a particular gem
    private bool MatchType(CellOffset offset, int type)
    {
        // make sure col and row aren't beyond the limit
        if ((offset.Col < 0) || (offset.Col >= width) || (offset.Row < 0) || (offset.Row >= height))
            return false;
        //print(string.Format("col: {0} row: {1}", offset.Col, offset.Row));
        return (grid[offset.Col, offset.Row].PieceType == type);

    }


    // callback from piece on click
    public void ClickPiece(Piece piece)
    {
        if (state == State.Waiting)
        {
            //print(string.Format("pieceSelected {0}, {1}", piece.Col, piece.Row));

            // first piece i've selected
            if (firstPiece == null)
            {
                firstPiece = piece;
                selectParticle.transform.position = piece.transform.position;
                selectParticle.gameObject.SetActive(true);
                audio.PlayOneShot(gemSelect);
            }
            // I have a piece selected and I've selected it again
            else if (piece == firstPiece)
            {
                firstPiece = null;
                selectParticle.gameObject.SetActive(false);
            }
            // I have selected another 
            else
            {
                // same row, one column over
                if ((firstPiece.Row == piece.Row) && (Math.Abs(firstPiece.Col - piece.Col) == 1))
                {
                    MakeSwap(firstPiece, piece);
                    firstPiece = null;

                }
                // same column, one row over
                else if ((firstPiece.Col == piece.Col) && (Math.Abs(firstPiece.Row - piece.Row) == 1))
                {
                    MakeSwap(firstPiece, piece);
                    firstPiece = null;
                }
                // bad move, reassign first piece
                else
                {
                    audio.PlayOneShot(gemSelect);
                    firstPiece = piece;
                    selectParticle.gameObject.SetActive(true);
                }

            }
        }
    }

    // swap two pieces
    private void MakeSwap(Piece piece1, Piece piece2)
    {
        selectParticle.gameObject.SetActive(false);

        // start animated swap of two pieces
        SwapPieces(piece1, piece2);

        // check to see if move was fruitful
        if (!ContainsMatch())
        {
            audio.PlayOneShot(invalidSwap);
            SwapPieces(piece1, piece2);
        }
        else
        {
            audio.PlayOneShot(gemSwap);

            state = State.Swapping;
        }

    }


    // swap two pieces
    private void SwapPieces(Piece piece1, Piece piece2)
    {
        // swap row and col values
        int tempCol = piece1.Col;
        int tempRow = piece1.Row;
        piece1.Col = piece2.Col;
        piece1.Row = piece2.Row;
        piece2.Col = tempCol;
        piece2.Row = tempRow;

        // swap grid positions
        grid[piece1.Col, piece1.Row] = piece1;
        grid[piece2.Col, piece2.Row] = piece2;

    }


    // if any pieces are out of place, move them a step closer to being in place
    // happens when pieces are swapped, or they are dropping
    private void MovePieces()
    {
        bool madeMove = false;
        float moveAmount = moveSpeed * Time.deltaTime;
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                Piece tile = grid[col, row];

                if (grid[col, row] != null)
                {
                    Vector3 targetPos = GetTargetPos(col, row);
                    Vector3 offset = targetPos - tile.transform.localPosition;

                    // piece is almost in place - snap
                    if (offset.sqrMagnitude < moveAmount * moveAmount)
                    {
                        tile.transform.localPosition = targetPos;
                    }
                    else
                    {
                        // move towards destination
                        offset.Normalize();
                        tile.transform.localPosition += offset * moveAmount;
                        madeMove = true;
                    }
                }
            }
        }

        // if all movement complete
        if (!madeMove)
        {
            state = State.Matching;
            FindAndRemoveMatches();
        }
    }


    // gets matches and removes them, applies points
    public void FindAndRemoveMatches()
    {
        // get list of matches
        var matches = AllMatches();

        // foreach match
        foreach (var match in matches)
        {
            int points = (match.Count - 1) * 50;

            foreach (var cell in match)
            {

                // add to the score
                AddScore(points);

                // create a point burst
                //audio.PlayOneShot(gemBurst);
                var pointBurst = Instantiate(pointBurstPrefab);
                pointBurst.SetText(points.ToString());
                pointBurst.transform.parent = transform;
                pointBurst.transform.localPosition = GetTargetPos(cell.Col, cell.Row);

                if (grid[cell.Col, cell.Row] != null)
                {
                    grid[cell.Col, cell.Row].Bust();
                    grid[cell.Col, cell.Row] = null;
                }
                AffectAbove(cell);
            }

            var matchBurst = Instantiate(pointBurstPrefab);
            matchBurst.SetText(MatchEffect(match[0].PieceType, match.Count - 2));
            matchBurst.SetColour(announceColour);
            matchBurst.transform.parent = transform;
            Vector3 tmpv3 = GetTargetPos(3, 8);
            tmpv3.x += 0.5f;
            matchBurst.transform.localPosition = tmpv3;
            //print(string.Format("Match: {0}!", MatchEffect(match[0].PieceType)));


        }

        // add any new piece to top of board
        AddNewPieces();

        // no matches found, maybe the game is over?
        if (matches.Count == 0)
        {
            if (!ContainsPossibles())
            {
                EndGame(3);
            }
            else
            {
                state = State.Waiting;
            }
        }
    }


    // tell all pieces above this one to move down
    private void AffectAbove(Piece piece)
    {
        state = State.Dropping;
        int col = piece.Col;
        for (int row = piece.Row + 1; row < height; row++)
        {
            if (grid[col, row] != null)
            {
                grid[col, row].Row--;
                grid[col, row - 1] = grid[col, row];
                grid[col, row] = null;
            }
        }
    }


    // if there are missing pieces in a column, add one to drop
    private void AddNewPieces()
    {
        for (int col = 0; col < width; col++)
        {
            for (int row = 0; row < height; row++)
            {
                int first = 0;
                if (grid[col, row] == null)
                {
                    Piece newPiece = AddPiece(col, row);
                    newPiece.PieceType = rand.Next(Piece.MAX_PIECES);
                    newPiece.transform.localPosition += Vector3.up * (first - height) * yOffset;
                    state = State.Dropping;
                }
                else
                {
                    first++;
                }
            }
        }

    }

    // changed to add condition to indicate to player why game ended, and display this on the UI
    private void EndGame(int condition)
    {
        state = State.Ended;
        //TogglePause();
        Time.timeScale = 0f;
        Overlay(condition == 0 ? "GAME OVER\nYOU WIN!"
               : condition == 1 ? "GAME OVER\nYOU LOSE!"
               : "GAME OVER\nNO MATCHES REMAINING");
        overlayButton.interactable = true;
        var ta = overlayButton.GetComponentInChildren<UnityEngine.UI.Text>();
        ta.text = "Menu";
    }

    private void AddScore(int points)
    {
        score += points;
        scoreText.text = score.ToString();
    }

    #endregion

    #region Dinofite methods (Match effects. Damage, Projectile Spawning)
    /* all new methods for running the Dinofite game rules on top of the Match3 game */

    // Runs once/sec, to decrement player health by boss damage amount
    private void DamagePlayer()
    {
        // if shielded don't decrement health
        if (shieldCount <= 0)
        {
            // set UI shield to empty
            shieldIndicator.fillAmount = 0f;
            // reduce player health
            playerHealth -= bossDPS;
            // update UI 
            playerHealthText.text = playerHealth.ToString();

            // if player ran out of health, end the game
            if (playerHealth <= 0)
            {
                playerHealthText.text = "0";
                // Endgame param: 1 - Loss
                EndGame(1);
            }
            // if health has dropped below 25% for the first time, increase music speed, (pitch)
            else if ((playerHealth < playerMaxHealth / 4) && (firstTimeHealthDrop))
            {
                backgroundMusic.pitch = 1.25f;
                firstTimeHealthDrop = true;
            }
        }
        bossDPSRamp += bossInitDPS;
        if (bossDPSRamp >= bossDPSRampRate)
        {
            // Fire a boss type projectile
            FireProjectile(0);
            if (bossDPS < (bossInitDPS > 0 ? bossInitDPS * 10 : 10))
            {
                bossDPS++;
                bossDPSRamp = 0;
                // check if initial bossdps is > 0 then if current is > 0 and update UI accordingly
                bossDPSText.text = (bossInitDPS > 0 ? ((bossDPS / bossInitDPS) * 10).ToString() + "%" : (bossDPS > 0 ? (bossDPS * 10).ToString() + "%" : "0%"));

                // if boss special bar is full (max DPS), start particle effects
                if (bossDPS >= (bossInitDPS > 0 ? bossInitDPS * 10 : 10))
                {

                    bossSpecialParticle.Play();
                }
            }
        }
    }

    /* helper for generating projectiles
        source = 0 - generate boss projectile
        source > 0 - generate player projectile
    */
    private void FireProjectile(int source)
    {
        Projectile proj = Instantiate(projectilePrefab);
        proj.transform.parent = transform;
        if (source == 0)
        {
            proj.transform.position = boss.transform.position;
        }
        else
        {
            proj.transform.position = player.transform.position;
        }
        proj.ProjectileType = source;
        proj.gameObject.SetActive(true);

    }

    #region Match Effect Methods

    // called when a match is found, calls the appropriate method to run game behaviour for this kind of match
    private string MatchEffect(int type, int multiplier)
    {
        switch (type)
        {
            case 0: //sword
                audio.PlayOneShot(sword);
                return (DamageBoss(multiplier));
            case 1: //potion
                audio.PlayOneShot(potion);
                return (HealPlayer(multiplier));
            case 2: //crossed
                audio.PlayOneShot(reset);
                return (ResetBossDPS());
            case 3: //claw
                audio.PlayOneShot(laser);
                return (SpecialAttack(multiplier));
            case 4: //coin
                audio.PlayOneShot(coin);
                return (GainGold(multiplier));
            case 5: //shield
                audio.PlayOneShot(shield);
                return (ActivateShield(multiplier));
            default:
                DamageBoss(multiplier);
                return ("Default?");
        }
    }

    // Swords match - player damages boss
    private String DamageBoss(int multi)
    {
        FireProjectile(1);
        bossHealth -= playerAttack * multi;
        bossHealthText.text = bossHealth.ToString();

        //if boss ran out of health, player wins
        if (bossHealth <= 0)
        {
            //Endgame param = 0 - WIN
            EndGame(0);
        }
        return ("Attack!");
    }

    //Potion match - player gains health
    private String HealPlayer(int multi)
    {
        playerHealth += baseHeal * multi;

        // check can't go above maximum health
        if (playerHealth > playerMaxHealth)
        {
            playerHealth = playerMaxHealth;
        }
        playerHealthText.text = playerHealth.ToString();
        return ("Heal!");
    }

    // crossed staves match - resets the bosses DPS to initial amount
    private String ResetBossDPS()
    {
        bossDPS = bossInitDPS;
        bossDPSRamp = 0;
        bossDPSText.text = (bossInitDPS > 0 ? ((bossDPS / bossInitDPS) * 10).ToString() + "%" : "0%");

        // special bar empty, boss DPS reset, stop particles
        bossSpecialParticle.Stop();
        return ("Reset!");
    }

    // coin match - player gains gold
    private String GainGold(int multi)
    {
        playerGold += playerGoldGain * multi;
        playerGoldGain += playerGoldGainRate;
        playerGoldText.text = playerGold.ToString();
        return ("Gold!");
    }

    // claw match - charge or discharge special attack
    private String SpecialAttack(int multi)
    {
        FireProjectile(1);
        if (playerSpecialCharge >= 100)
        {
            spawnSpecialShots = specialShots;
            bossHealth -= (playerSpecial * playerSpecialModifier * multi);
            bossHealthText.text = bossHealth.ToString();
            playerSpecialCharge = 0;
            playerSpecialText.text = "0%";
            playerSpecialParticle.Stop();
            if (bossHealth <= 0)
            {
                EndGame(0);
            }
            return "Special Attack!!";
        }
        else
        {
            playerSpecialCharge += playerSpecialPerMatch * multi;
            if (playerSpecialCharge >= 100)
            {
                playerSpecialCharge = 100;
                playerSpecialParticle.Play();
            }
            playerSpecialText.text = string.Format("{0} %", playerSpecialCharge.ToString());
            return "Special Charging!!";
        }
    }

    private String ActivateShield(int multi)
    {
        shieldCount += shieldAmount * multi;
        shieldMax = (int)shieldCount;
        shieldIndicator.fillAmount = 1f;
        return "Shielded!";
    }


    #endregion

    #endregion

    #region Misc Helpers (Pause, FillBars, Overlay)
    // turn overlay UI on/off
    private void ToggleOverlay()
    {
        Color c = overlayText.color;
        if (overlayOn == true)
        {
            overlayOn = false;
            c.a = 0f;
            overlayText.color = c;
            c = overlay.color;
            c.a = 0f;
        }
        else
        {
            overlayOn = true;
            c.a = 1f;
            overlayText.color = c;
            c = overlay.color;
            c.a = 0.5f;
        }
        overlay.color = c;
    }

    private void Overlay(String text)
    {
        overlayOn = true;

        Color c = overlay.color;
        c.a = 0.5f;
        overlay.color = c;

        c = overlayText.color;
        c.a = 1f;
        overlayText.color = c;
        overlayText.text = text;
    }

    private void TogglePause()
    {
        // turn overlay on/off
        ToggleOverlay();
        //change timescale to pause/start game execution
        if (state == State.Paused)
        {
            Time.timeScale = 1f;
            state = pauseState;
        }
        else
        {
            Time.timeScale = 0f;
            pauseState = state;
            state = State.Paused;
        }

    }

    // healper to fill UI indicators
    private void FillAllIndicators()
    {
        shieldCount -= 1.0f * Time.deltaTime;
        if (shieldCount < 0) { shieldCount = 0; }
        FillIndicator(playerSpecialBar, playerSpecialCharge, 100f);
        FillIndicator(playerHealthBar, playerHealth, playerMaxHealth);
        FillIndicator(bossSpecialBar, bossDPS / bossInitDPS, 10f);
        FillIndicator(bossHealthBar, bossHealth, bossMaxHealth);
        FillIndicator(shieldIndicator, shieldCount, shieldMax);
    }

    // helper to give UI indicator changes a fluid transition
    private Boolean FillIndicator(UnityEngine.UI.Image img, float tar, float upper)
    {
        float curr = img.fillAmount;
        float proper = tar / upper;
        if (Math.Abs(curr - proper) > 0.001f)
        {
            img.fillAmount = Mathf.Lerp(curr, proper, 3f * Time.deltaTime);
            return true;
        }
        img.fillAmount = proper;
        return false;
    }

    #endregion
}
