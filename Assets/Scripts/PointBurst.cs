﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A 'burst' of points that scales and fades, and deletes itself when done
/// </summary>
public class PointBurst : MonoBehaviour {

    // the total time for the point burst
    public float time = 1;
    public float finalScale = 2;
    public float finalRise = 3;
    private float elapsed = 0;

    // cache commonly used components
    private TextMesh textMesh;
    private MeshRenderer textRenderer;
	// Use this for initialization
	void Awake () {
        // cache the text mesh
        textMesh = GetComponentInChildren<TextMesh>();
        textRenderer = GetComponentInChildren<MeshRenderer>();
	}


    public void SetText(string text)
    {
        textMesh.text = text;
    }

    public void SetColour(Color c)
    {
        textMesh.color = c;
    }

	// Update is called once per frame
	void Update () {
        // increment elapsed time
        elapsed += Time.deltaTime;

        // how far are we through the animation
        float tau = elapsed / time;

        // set the alpha fade
        textRenderer.material.color = new Color(1, 1, 1, Mathf.Lerp(1, 0, tau));
        
        // set the scale
        textMesh.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * finalScale, tau);

        // set the position
        textMesh.transform.localPosition = Vector3.Lerp(Vector3.zero, Vector3.up * finalRise, tau);

        // kill this pointburst when complete
        if (tau > 1)
        {
            GameObject.Destroy(gameObject);
        }
	}
}
