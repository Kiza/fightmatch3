﻿using UnityEngine;

public class Projectile : MonoBehaviour {
    public Sprite[] sprites;
    public Vector3 direction;
    public GameObject[] origins;
    public float lifetime;
    public int ProjectileType
    {
        get
        {
            return projectileType;
        }

        set
        {
            if (value < sprites.Length)
            {
                spriteRenderer.sprite = sprites[value];
                projectileType = value;
                if (value > 0)
                {
                    projectileDirection = new Vector3(2f * 0.15f, (Random.value-0.5f)*0.08f,0f);
                    
                }else
                {
                    projectileDirection = new Vector3(-2f*0.15f, (Random.value-0.5f)*0.08f,0f);
                }
            }
        }
    }

    private Vector3 projectileDirection;
    private SpriteRenderer spriteRenderer;
    private int projectileType;

    // Use this for initialization
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    private void Start () {
	}

    // Update is called once per frame
    private  void Update () {
        this.transform.position += projectileDirection;
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            this.Destroy();
        }
	}

    public void Destroy()
    {
        Destroy(gameObject);
    }
}