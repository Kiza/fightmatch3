﻿using UnityEngine;

public class Piece : MonoBehaviour {
    /*
    ATM 
    green = attack
    blue = special
    orange = reset
    red = heal
    yellow = gold
    others = attack default
    */
    public const int MAX_PIECES = 6;// 7;
    public Sprite[] sprites;
    public int Row { get; set; }
    public int Col { get; set; }
     public int PieceType { 
         get 
         { 
             return pieceType;
         }
 
         set
         {
             if (value < sprites.Length)
             {
                 spriteRenderer.sprite = sprites[value];
                 pieceType = value;
             }
         } 
     }

    // cached components
    private SpriteRenderer spriteRenderer;

    // this piece
    private int pieceType;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }


    public void OnMouseUp()
    {
        SendMessageUpwards("ClickPiece", this);
    }

    public void Bust()
    {
        Destroy(gameObject);
    }
}
